use std::time::Duration;
use std::thread;
use std::fs::File;
use std::io::{Write, BufWriter};

use syscall::{O_RDONLY, O_NONBLOCK, O_CLOEXEC};

fn daemon(daemon: redox_daemon::Daemon) -> ! {
    const MIB: usize = 1024 * 1024;
    const N: usize = 4;

    let mut buf = vec![0_u8; 64 * MIB];

    let sockets = (0..N).map(|i| syscall::open(format!("debug:profiling-{i}"), O_RDONLY | O_NONBLOCK | O_CLOEXEC).expect("profiled: failed to open profiling pipe")).collect::<Vec<_>>();
    let mut out = BufWriter::new(File::create("file:/root/profiling.txt").unwrap());

    syscall::setrens(0, 0).expect("profiled: failed to enter null namespace");
    daemon.ready().expect("profiled: failed to notify parent");

    loop {
        for (cpu, socket) in sockets.iter().enumerate() {
            let n = syscall::read(*socket, &mut buf).unwrap();
            println!("profiled: {n} bytes from CPU{cpu}");
            let data = &buf[..n];
            let mut words: &[usize] = bytemuck::cast_slice(data);
            let Some(first_new) = words.iter().position(|w| w & 1 << 63 == 0) else {
                continue;
            };
            if first_new != 0 {
                eprintln!("profiled: lost {first_new} words");
            }
            words = &words[first_new..];

            while !words.is_empty() {
                let stack = &words[2..];
                let next_new = stack.iter().position(|w| w & 1 << 63 == 0).unwrap_or(stack.len());
                write!(out, "{:x} {:x} {:x}", cpu, words[0] | (1 << 63), words[1]).unwrap();
                for addr in &stack[..next_new] {
                    write!(out, " {:x}", addr).unwrap();
                }
                writeln!(out).unwrap();
                words = &stack[next_new..];
            }
        }
        out.flush().unwrap();
        thread::sleep(Duration::from_secs(1));
    }
}

fn main() {
    redox_daemon::Daemon::new(daemon).expect("profiled: failed to daemonize");
}
